﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Assignment4_Api.Models;
using System.Web.Http.Cors;
using System.IO;
using Newtonsoft.Json;



// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Assignment4_Api.Controllers
{

    [Route("api/hotel")]
    public class HotelController : Controller
    {
        public Hotel hotel = Hotel.GetHotel();

		// GET: /<controller>/
		public IActionResult Index()
		{
			
            return View();
        }

        [HttpPost]
        [Route("search")]
		[EnableCors(origins: "http://localhost:56858", headers: "*", methods: "*")]
        public List<Room> Search()
        {
			var str = "";
			//Filter filter = new Filter();
			using (StreamReader reader = new StreamReader(Request.Body))
			//using(var json = new JsonTextReader(reader))
            {
				str = reader.ReadToEnd();
				//if (json != null)
				//{
				//	filter = _serializer.Deserialize<Filter>(json);
				//}
				//else
					//return null;
            }
			//Console.WriteLine(str);
			Filter filter = JsonConvert.DeserializeObject<Filter>(str);
			try
			{
				return hotel.searchRoom(filter);
			}
			catch(ArgumentException e){
				Console.WriteLine(e.Message);
				return null;
			}
        }

        [HttpPost]
        [Route("reserve")]
		[EnableCors(origins: "http://localhost:56858", headers: "*", methods: "*")]
		public bool Reserve()
        {
			var str = "";
            using (StreamReader reader = new StreamReader(Request.Body))
            {
                str = reader.ReadToEnd();
            }
			BookRequest bq = JsonConvert.DeserializeObject<BookRequest>(str);
			Console.WriteLine("~~~~~~~~~~~~room number from json"+bq.roomNumber+"~~~~~~move in date from json"+bq.moveInDate+"~~~~~move out date from json"+bq.moveOutDate);
			Room room = hotel.getRoomByNumber(bq.roomNumber);
			DateTime mid = Hotel.parseDate(bq.moveInDate);
			DateTime mod = Hotel.parseDate(bq.moveOutDate);
			int duration = Hotel.distancebet(mid,mod);
			if (hotel.CheckAvail(room, bq.moveInDate, bq.moveOutDate))
			{
				for (int i = 0; i < duration;i++)
				    hotel.ReserveRoom(room, Hotel.parseDate(bq.moveInDate).AddDays(i));
				return true;
			}
            return false;
        }

		[HttpGet]
        [Route("getall")]
		[EnableCors(origins: "http://localhost:56858", headers: "*", methods: "*")]
		public List<Room> GetAll(){
            //string str = "";
            //using (StreamReader reader = new StreamReader(Request.Body))
            //{
            //    str = reader.ReadToEnd();
            //}

            //return str;
            return hotel.roomList;
        }


    }
}