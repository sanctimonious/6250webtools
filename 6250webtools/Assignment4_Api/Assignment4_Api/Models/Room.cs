﻿using System;
namespace Assignment4_Api.Models
{
	
	public class Room
	{
		public int floor { get; set; }
		public int id { get; set; }
		public int capacity;
		public string roomtype { get; set; }
		public string bathroomUrl { get; private set; }
		public string restroomUrl { get; private set; }
		public string barUrl { get; private set; }
		public Reservation reservation { get; private set; }
		private static int no = 1;
		public string roomNumber;

		public Room(int floor,int capacity,string roomtype,string restroomUrl,string bathroomUrl,string barUrl){
			reservation = new Reservation();
			this.floor = floor;
			this.id = no++;
			this.roomtype = roomtype;
			this.bathroomUrl = bathroomUrl;
			this.bathroomUrl = barUrl;
			this.restroomUrl = restroomUrl;
			this.capacity = capacity;
			roomNumber = floor + "-" + id;
		}

    }
}
