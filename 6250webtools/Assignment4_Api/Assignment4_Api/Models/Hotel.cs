﻿using System;
using System.Collections.Generic;

namespace Assignment4_Api.Models
{
    public class Hotel
    {
		public List<Room> roomList { get; set; }
		private static Hotel hotelrefer = null;
        
		//arg. room: the room to reserve...date: start date
		public void ReserveRoom(Room room,DateTime date)
		{
			if(room.reservation.table[date] == null)
				room.reservation.table[date] = true;
		}
        
		//arg. room: the room to check.. date: start date..duration: how many days
		public bool CheckAvail(Room room,string moveInDate,string moveOutDate)
		{         
			DateTime startdate = parseDate(moveInDate);
			DateTime enddate = parseDate(moveOutDate);
			//DateTime dateTime = date;
			int duration = Math.Abs(distancebet(startdate,enddate));
			int dur = 0;
			DateTime dateTime = startdate;
			while(dur <= duration+1){
				if (room.reservation.table[dateTime] != null)
					return false;
				dur++;
				dateTime = dateTime.AddDays(1);
				//Console.WriteLine();
			}
			return true;
		}

		private Hotel(){
			roomList = new List<Room>(){
				new Room(1,2,"Standard","restroom url","bathroom url","bar url"),
				new Room(1,3,"Luxury","restroom url","bathroom url","bar url"),
				new Room(1,4,"Family","restroom url","bathroom url","bar url"),
				new Room(2,2,"Standard","restroom url","bathroom url","bar url"),
				new Room(2,3,"Luxury","restroom url","bathroom url","bar url"),
				new Room(2,4,"Family","restroom url","bathroom url","bar url"),
				new Room(3,2,"Standard","restroom url","bathroom url","bar url"),
				new Room(3,3,"Luxury","restroom url","bathroom url","bar url"),
				new Room(3,4,"Family","restroom url","bathroom url","bar url"),
				new Room(4,2,"Luxury","restroom url","bathroom url","bar url"),
				new Room(4,3,"Family","restroom url","bathroom url","bar url"),
				new Room(4,4,"Deluxe","restroom url","bathroom url","bar url"),
				new Room(5,2,"Luxury","restroom url","bathroom url","bar url"),
				new Room(5,3,"Family","restroom url","bathroom url","bar url"),
				new Room(5,4,"Deluxe","restroom url","bathroom url","bar url")
			};
		}

		public Room getRoomByNumber(string roomNumber){
			string[] str = roomNumber.Split("-");
			int floor = -1;
			int.TryParse(str[0],out floor);
			int id = -1;
			int.TryParse(str[1],out id);

			if (floor < 0 || id < 0)
				return null;

			foreach(Room r in this.roomList){
				if (r.floor == floor && r.id == id)
					return r;
			}
			return null;
		}
        
		public static Hotel GetHotel(){
			if (hotelrefer == null)
			{
				hotelrefer = new Hotel();
				return hotelrefer;
			}
			else
				return hotelrefer;
		}

		public List<Room> searchRoom(Filter filter){
			List<Room> resultList = new List<Room>();
			foreach (Room r in roomList){
				if(r.capacity == filter.beds 
				   && this.CheckAvail(r,filter.moveInDate,filter.moveOutDate))
				{
					resultList.Add(r);
				}
			}
			return resultList;
		}

		public static DateTime parseDate(string date){
            int[] dateint = new int[3];         
            string[] datestring = date.Split("/");

            try
            {
                for (int i = 0; i < 3; i++)
                {
                    dateint[i] = int.Parse(datestring[i]);
                }
            }
            catch (Exception e)
            {

            }
			try
			{
				DateTime newDate = new DateTime(dateint[2], dateint[1], dateint[0]);
				return newDate;
			}
			catch(ArgumentOutOfRangeException e){
				throw new ArgumentException("invalid date infomation!");
			}
		}

		public static int distancebet(DateTime t1, DateTime t2)
        {
            if (t1.Year == t2.Year)
            {
                return t2.DayOfYear - t1.DayOfYear;
            }
            else if (t1.Year % 4 != 0)
                return t2.DayOfYear + 365 - t1.DayOfYear;
            else
                return t2.DayOfYear + 366 - t1.DayOfYear;
        }

        
    }

	public class Filter{
        
		public int beds;
		public string moveInDate;
		public string moveOutDate;

		public Filter(){}
	}

	//public class Date{
		//public int year { get; set; }
		//public int month { get; set; }
		//public int day { get; set; }

		//public Date(int y,int m,int d){
		//	year = y;
		//	month = m;
		//	day = d;         
		//}

		//public Date(){}

		//public static int compareTo(Date d1,Date d2){
		//	if(d1.year == d2.year){
		//		if (d1.month == d2.month)
		//			return Math.Abs(d1.day - d2.day);
		//		else (d1.month < d2.month)
		//			return d2.day + 30 - d1.day;
		//	}
		//	else
		//		return 
		//}



}
