﻿using System;
using System.Collections;

namespace Assignment5_Angular.Models
{
    public class Room
    {
		public int floor { get; set; }
        public int id { get; set; }
        public int capacity;
        public string roomtype { get; set; }
        public string bathroomUrl { get; private set; }
        public string restroomUrl { get; private set; }
        public string barUrl { get; private set; }
        public Hashtable reservation { get; private set; }
        private static int no = 1;
        public string roomNumber;

        public Room(int floor, int capacity, string roomtype, string restroomUrl, string bathroomUrl, string barUrl)
        {
            reservation = new Hashtable();
            this.floor = floor;
            this.id = no++;
            this.roomtype = roomtype;
            this.bathroomUrl = bathroomUrl;
            this.bathroomUrl = barUrl;
            this.restroomUrl = restroomUrl;
            this.capacity = capacity;
            roomNumber = floor + "-" + id;
        }

		public bool resetRoom(){
			this.reservation = new Hashtable();
			return true;
		}
    }
}
