﻿import { Http, Headers, Response, Request, RequestMethod, URLSearchParams, RequestOptions } from "@angular/http"; 
import { Component } from '@angular/core';
//import { Router } from '@angular/router';


@Component({
    selector: 'booked',
    template: require('./booked.component.html')
})

export class bookedComponent {
    public roomList: string;

    constructor(private http: Http) {
        this.roomList = '';
        this.getAllBookedRoom();
    }

    getAllBookedRoom() {
        
        //$.ajaxSetup({
        //    async: false
        //});

        this.http.get('http://localhost:5000/api/hotel/getallbooked').subscribe(result => {
            this.roomList = result.json();
        });        
        //return this.roomList;
        //alert(this.roomList);

        return this.roomList;
    }
    delete(id:string) {
        //var btn:HTMLButtonElement|null = <HTMLButtonElement>document.getElementById(id);
        this.http.delete('http://localhost:5000/api/hotel/delete/'+id).subscribe(result => {
            alert(result);
            //return result;
        })
    }    
    createRoom() {
        let capacity: number = 5;
        let roomType: string = "standard";
        let room: any = { "capacity": capacity, "roomType": roomType };
        this.http.put('http://localhost:5000/api/hotel/putRoom', room).subscribe(result => {
            if (result) {
                alert("create successfully");
            }
        });
    }
}