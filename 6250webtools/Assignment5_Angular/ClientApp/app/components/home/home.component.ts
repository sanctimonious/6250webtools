import { Component } from '@angular/core';
import { Http } from '@angular/http';

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})
export class HomeComponent {
    url: any;
    constructor(private http : Http) {
    }

    getBg() {
        this.http.get("").subscribe(res => {
            this.url = res;
        });
    }
}
