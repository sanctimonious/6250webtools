﻿import { Http,Headers, Response, Request, RequestMethod, URLSearchParams, RequestOptions } from "@angular/http"; 

import { Component, Injectable, Inject, EventEmitter, Input, OnInit, Output, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser'; 

@Component({
    selector:'hotel',
    template: require('./hotel.component.html')
    })

export class hotelComponent {

    public roomDetailListJson : string;
    public mid :string;
    public mod :string;

    constructor(public http: Http) {
        this.roomDetailListJson = '';
        this.mid = "";
        this.mod = "";
        this.getAllRoom();
    }

    getAllRoom() {
        this.http.get('http://localhost:5000/api/hotel/getall').subscribe(result => {
            this.roomDetailListJson = result.json();
        });
        return this.roomDetailListJson;
    }

    getSearchResult() {
        var midEle:HTMLInputElement|null = <HTMLInputElement>document.getElementById("movein");
        var modEle:HTMLInputElement|null = <HTMLInputElement>document.getElementById("moveout");
        var bednumsEle:HTMLInputElement|null = <HTMLInputElement>document.getElementById("bednums");
        var filter:String;
        if(midEle!=null && modEle!=null && bednumsEle!=null){
            filter = JSON.stringify({
                moveInDate:midEle.value,
                moveOutDate:modEle.value,
                beds:bednumsEle.value,
                });
            this.http.post('http://localhost:5000/api/hotel/search',filter).subscribe(result =>{
                this.roomDetailListJson = result.json();
            });
        }
    }


    bookRoom(id : string) {
        var tableRow:HTMLTableRowElement|null=<HTMLTableRowElement>document.getElementById(id);
        var midEle:HTMLInputElement|null = <HTMLInputElement>document.getElementById("movein");
        var modEle:HTMLInputElement|null = <HTMLInputElement>document.getElementById("moveout");
        var bookRequest:String;
        if(midEle!=null && modEle!=null){
            bookRequest = JSON.stringify({
                moveInDate:midEle.value,
                moveOutDate:modEle.value,
                roomNumber: id
            })
            this.http.post('http://localhost:5000/api/hotel/reserve', bookRequest).subscribe(result => 
            {
                if (result && tableRow!=null) {
                    tableRow.remove();
                }
            });
        }
    }






}