import { Component } from '@angular/core';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
    selector: 'admin',
    template: require('./Admin.component.html')
})
export class AdminComponent {
    constructor(private router: Router) { }
    public currentCount = 0;

    public incrementCounter() {
        this.currentCount++;
    }
    public validateAndGo() {
        var uname = $('#uname').val();
        var pwd = $('#pwd').val();

        if (uname == null || uname == '' || pwd == '' || pwd == null) {
            alert("username or password cannot be null!");
        }

        if (uname == 'han' && pwd == 'han') {
            this.router.navigate(['booked']);
        }
    }
}
