﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Assignment4.Controllers
{
    public class HotelController : Controller
    {
        // GET: /<controller>/
        public IActionResult Hotel()
        {
            return View();
        }
        [Route("/Menu")]
        public IActionResult Menu()
        {
            return View();
        }
        [Route("/About")]
        public IActionResult AboutUs()
        {
            return View();
        }
        [Route("/Reserve")]
        public IActionResult Reserve()
        {
            return View();
        }
    }
}
